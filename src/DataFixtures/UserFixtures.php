<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 10; $i++){
            $user = new User();
            $user->setFirstname("user".$i)
                ->setLastname("user".$i)
                ->setEmail("user".$i."@gmail.com")
                ->setCreatedAt(new \DateTime())
                ->setAdress("1 rue du user".$i)
                ->setCountry("France")
                ->setSubscription("test");
            $manager->persist($user);
        }

        $manager->flush();
    }
}