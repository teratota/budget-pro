<?php

namespace App\Manager;
use App\Entity\Card;
use App\Repository\CardRepository;

class CardManager
{
    private $cardRepository;
    /**
     * CardManager constructor.
     */
    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }
    public function getAll(): ?array {
        return $this->cardRepository->findAll();
    }
    public function getCard(int $id) : ?Card
    {
        return $this->cardRepository->find($id);
    }
}