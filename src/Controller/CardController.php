<?php

namespace App\Controller;

use App\Entity\Card;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Repository\CardRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class CardController extends AbstractFOSRestController
{
    private $CardRepository;
    public function __construct(CardRepository $CardRepository, EntityManagerInterface $em)
    {
        $this->CardRepository = $cardRepository ;
        $this->em = $em;
    }

    /**    
     *  @Rest\Get("/api/card/{name}")
     * @Rest\View(serializerGroups={"Card"})   
     * @IsGranted("ROLE_ADMIN")    
    */
    public function getApiCard(Card $card){
        return $this->view($card);
    }
    
    /**     
     * @IsGranted("ROLE_ADMIN") 
     * @Rest\Get("/api/card")   
    */
    public function getApiCards(){
        $card = $this->cardRepository ->findAll();
        return $this->view($card);
    }
    
    /**    
     * @Rest\Post("/api/card")
     * @ParamConverter("card", converter="fos_rest.request_body")
     * @IsGranted("ROLE_ADMIN") 
    */
    public function postApiCard(Card $card){
        $this->em->persist($card);
        $this->em->flush();
        return $this->view($card);
    }
    
    /**    
     * @Rest\Patch("/api/card/{name}")   
     * @IsGranted("ROLE_ADMIN")  
    */
    public function patchApiCard(Card $card){}
    
}