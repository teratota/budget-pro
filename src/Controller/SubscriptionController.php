<?php

namespace App\Controller;

use App\Entity\Subscription;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Repository\SubscriptionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class SubscriptionController extends AbstractFOSRestController
{
    private $SubscriptionRepository;
    public function __construct(SubscriptionRepository $SubscriptionRepository, EntityManagerInterface $em)
    {
        $this->SubscriptionRepository = $SubscriptionRepository ;
        $this->em = $em;
    }

    /**    
     *  @Rest\Get("/api/subscription/{name}")   
     * @Rest\View(serializerGroups={"Subscription"})  
    */
    public function getApiSubscription(Subscription $subscription){
        return $this->view($subscription);
    }
    
    /**     
     * @Rest\Get("/api/subscription")   
    */
    public function getApiSubscriptions(){
        $subscription = $this->SubscriptionRepository ->findAll();
        return $this->view($subscription);
    }
    
    /**    
     *  @IsGranted("ROLE_ADMIN") 
     * @Rest\Post("/api/subscription")
     * @ParamConverter("subscription", converter="fos_rest.request_body")
     * @Rest\View(serializerGroups={"Subscription"}) 
    */
    public function postApiSubscription(Subscription $subscription){
        $this->em->persist($subscription);
        $this->em->flush();
        return $this->view($subscription);
    }
    
    /**    
     * @IsGranted("ROLE_ADMIN") 
     * @Rest\Patch("/api/subscription/{name}")
     * @Rest\View(serializerGroups={"Subscription"})     
    */
    public function patchApiSubscription(Subscription $subscription){}
    
}